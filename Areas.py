import math
def area_esfera(Radio):
    """ Instituto Tecnológico de Costa Rica
        Ingeniería en COmputradores
        Programa:area_esfera
        Función: Calcula el área de una esfera
        Autores: Gerald Salazar Elizondo(2018099115) y Brayan Rodríguez Villalobos
        Versión:1.0
        Entradas: Radio de una esfera
        Restricciones: Debe ser un número positivo
        Salidas: Aréa de una esfera
        Fecha:26/4/18
        """
    
    Area=4*math.pi*Radio**2#Area
    return Area


def area_piramide(Lado,Apotema):
    """ Instituto Tecnológico de Costa Rica
        Ingeniería en COmputradores
        Programa:area_piramide
        Función: Calcula el área de una piramide
        Autores: Gerald Salazar Elizondo(2018099115) y Brayan Rodríguez Villalobos
        Versión:1.0
        Entradas: Lado de la base de una piramide cuadrangular, y la apotema de esta
        Restricciones: Debe ser números positivos
        Salidas: Aréa de una piramide
        Fecha:26/4/18
        """
        Area=Lado*(2*Apotema+Lado)#Area
        return Area
    
